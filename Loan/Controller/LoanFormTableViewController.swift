//
//  LoanFormTableViewController.swift
//  Loan
//
//  Created by Faique Ali on 25/08/2020.
//  Copyright © 2020 Faique Ali. All rights reserved.
//

import UIKit
import Firebase

class LoanFormTableViewController: UITableViewController {

    @IBOutlet weak var borrowerNameTextField: UITextField!
    @IBOutlet weak var borrowerAddressTextField: UITextField!
    @IBOutlet weak var loanReqDescriptionTextField: UITextField!
    @IBOutlet weak var loanReqAmountTextField: UITextField!
    //Will Not Use
    @IBOutlet weak var lenderNameTextField: UITextField!
    @IBOutlet weak var lenderAddressTextField: UITextField!
    //------------
    @IBOutlet weak var loanReqDateTextField: UITextField!  //PK of LoanReqLender
    @IBOutlet weak var loanReqDeadlineTextField: UITextField!
    @IBOutlet weak var loanPaydayTextField: UITextField!
    @IBOutlet weak var reqLenderAmountTextField: UITextField!
    @IBOutlet weak var lenderBorrowerPercentageTxtField: UITextField!
    
    //LOAN ENTITY
    @IBOutlet weak var loanDateTxtField: UITextField!
    @IBOutlet weak var deadlineAgreedDataTxtField: UITextField!
    @IBOutlet weak var repaymentDateTxtField: UITextField! //PK Of Repayment
    @IBOutlet weak var repaymentAmountTxtField: UITextField!
    
    @IBOutlet weak var confirmBtn: UIButton!
    
    //Initialize Root Ref
    let rootRef = Database.database().reference()
    let childRef = Database.database().reference(withPath: "loan-management")
    
    //Initialize Child Ref
    let loanRequestRef = Database.database().reference(withPath: "loan-management").child("LoanRequest")
    let borrowerRef = Database.database().reference(withPath: "loan-management").child("Borrower")
    let addresseeRef = Database.database().reference(withPath: "loan-management").child("Addressee")
    let loanRequestLenderRef = Database.database().reference(withPath: "loan-management").child("LoanRequestLender")
    let lenderBorrowerRef = Database.database().reference(withPath: "loan-management").child("LenderBorrower")
    let lenderRef = Database.database().reference(withPath: "loan-management").child("Lender")
    let intermediaryRef = Database.database().reference(withPath: "loan-management").child("Intermediary")
    let repaymentRef = Database.database().reference(withPath: "loan-management").child("Repayment")
    let loanRef = Database.database().reference(withPath: "loan-management").child("Loan")
    let deadlineRef = Database.database().reference(withPath: "loan-management").child("Deadline")

    
    override func viewDidLoad() {
        super.viewDidLoad()

        confirmBtn.layer.cornerRadius = 15
    }
    
    //MARK:- IBAction
    
    @IBAction func saveLoanFormButton(_ sender: Any) {
        writeDataToFirestore()
    }
    
    //MARK:- Helper Methods
    func writeDataToFirestore(){
        
        //Addressee Entity
        let addresseeItemRef = self.addresseeRef.childByAutoId()
        let addresseeDict:[String: Any] = ["id": addresseeItemRef.key!, "name": borrowerNameTextField.text!, "address": borrowerAddressTextField.text!]
        addresseeItemRef.setValue(addresseeDict)
        
        //Borrower Entity
        let borrowerItemRef = self.borrowerRef.childByAutoId()
        let borrowerDict: [String: Any] = ["id": borrowerItemRef.key!,"addresseeID": addresseeItemRef.key!]
        borrowerItemRef.setValue(borrowerDict)
        
        //Loan Request Entity
        let loanReqItemRef = self.loanRequestRef.childByAutoId()
        let loanReqDict: [String: Any] = ["date": loanReqDateTextField.text! ,"borrowID": borrowerItemRef.key!, "deadline": loanReqDeadlineTextField.text! ,"amount": loanReqAmountTextField.text! ,"description": loanReqDescriptionTextField.text! ,"payday": loanPaydayTextField.text!]
        loanReqItemRef.setValue(loanReqDict)
        
        //Lender Entity
        let lenderItemRef = self.lenderRef.childByAutoId()
        let lenderDict: [String: Any] = ["id": lenderItemRef.key!,"addresseeID": addresseeItemRef.key!]
        lenderItemRef.setValue(lenderDict)
        
        //Lender_Borrower Entity
        let lenderBorrowerItemRef = self.lenderBorrowerRef.childByAutoId()
        let lenBorrDict: [String: Any] = ["borrowerID": borrowerItemRef.key!, "lenderID": lenderItemRef.key!, "loanDate": loanDateTxtField.text!, "percentage": lenderBorrowerPercentageTxtField.text!]
        lenderBorrowerItemRef.setValue(lenBorrDict)
        
        //Loan_Request_Lender Entity
        let loanReqLenItemRef = self.loanRequestLenderRef.childByAutoId()
        let loanReqLenDict: [String: Any] = ["loanRequestDate": loanReqDateTextField.text!, "lenderID": lenderItemRef.key!, "amount": reqLenderAmountTextField.text!]
        loanReqLenItemRef.setValue(loanReqLenDict)
       
        //Intermediary Entity
        let interItemRef = self.intermediaryRef.childByAutoId()
        let interDict: [String: Any] = ["id": interItemRef.key!, "addresseeID": addresseeItemRef.key!, "loanDate": loanDateTxtField.text!]
        interItemRef.setValue(interDict)
        
        //Deadline Entity
        let deadlineItemRef = self.deadlineRef.childByAutoId()
        let deadlineDict: [String: Any] = ["agreedDate": deadlineAgreedDataTxtField.text!]
        deadlineItemRef.setValue(deadlineDict)
        
        //Repayment Entity
        let repaymentItemRef = self.repaymentRef.childByAutoId()
        let repaymentDict: [String: Any] = ["date": repaymentDateTxtField.text! ,"amount": repaymentAmountTxtField.text!]
        repaymentItemRef.setValue(repaymentDict)
        
        //Loan Entity
        let loanItemRef = self.loanRef.childByAutoId()
        let loanDict: [String: Any] = ["date": loanDateTxtField.text!, "deadlineAgreedDate": deadlineAgreedDataTxtField.text!, "repaymentDate": repaymentDateTxtField.text!]
        loanItemRef.setValue(loanDict)
    }
    

}
