//
//  LoanReqListViewController.swift
//  Loan
//
//  Created by Faique Ali on 26/08/2020.
//  Copyright © 2020 Faique Ali. All rights reserved.
//

import UIKit
import Firebase

class LoanReqListViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var tblView: UITableView!
    
    //Initialize Child Ref
    let loanRequestRef = Database.database().reference(withPath: "loan-management").child("LoanRequest")
    let addresseeRef = Database.database().reference(withPath: "loan-management").child("Addressee")
    let loanRequestLenderRef = Database.database().reference(withPath: "loan-management").child("LoanRequestLender")
    
    //Array to store
    var loanRequestList = [LoanRequestModel]()
    var addresseeList = [AddresseeModel]()
    var loanReqLenderList = [LoanRequestLenderModel]()
    
    var amount = ["500000", "100000", "400000", "1000000", "900000"]
    var des = ["Taking loan for purchasing of a new house",
    "Taking loan for purchasing of a new car",
    "Taking loan for a startup business", "Taking loan for purchasing of a new house", "Taking loan for purchasing of a new flat"]
    var date = ["01-01-2020", "02-08-2019", "02-04-2017", "03-05-2016", "02-04-2015"]
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        getLoanReqDataFromFirebase()
        
    }
        
    //MARK:- Helper Methods
    func getLoanReqDataFromFirebase(){
        //get values from Addressee Entity
        addresseeRef.observe(.value, with: {(snapsot) in
            if snapsot.childrenCount > 0{
                for addressee in snapsot.children.allObjects as! [DataSnapshot] {
                    let addresseeObj = addressee.value as? [String: AnyObject]
                    let addresseeName = addresseeObj?["name"]
                    
                    let add1 = AddresseeModel(name: addresseeName as! String?)
                    self.addresseeList.append(add1)
                }
                self.tblView.reloadData()
            }
        })
        
        //get values from LoanRequest Entity
        loanRequestRef.observe(.value, with: {(snapshot) in
            if snapshot.childrenCount > 0 {
                for loanReq in snapshot.children.allObjects as! [DataSnapshot] {
                    let loanObj = loanReq.value as? [String: AnyObject]
                    let amount = loanObj?["amount"]
                    let description = loanObj?["description"]
                    
                    let loanReq1 = LoanRequestModel(amount: amount as? String, description: description as? String)
                    self.loanRequestList.append(loanReq1)
                }
                self.tblView.reloadData()
            }
        })
        
        //get values from LoanRequestLender Entity
        loanRequestLenderRef.observe(.value, with: {(snapshot) in
            if snapshot.childrenCount > 0 {
                for loanReqLen in snapshot.children.allObjects as! [DataSnapshot] {
                    let loanReqLenObj = loanReqLen.value as? [String: AnyObject]
                    let reqDate = loanReqLenObj?["loanRequestDate"]
                    
                    let loanReqLen1 = LoanRequestLenderModel(loanRequestDate: reqDate as? String)
                    self.loanReqLenderList.append(loanReqLen1)
                }
                self.tblView.reloadData()
            }
        })

    }

    //MARK:- Extension Methods
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return addresseeList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! LoanReqListTableViewCell
        
        let count = indexPath.row
        //Address Entity
        let a: AddresseeModel
        a = addresseeList[count]
        cell.lblBorrowerName.text = a.name
        cell.lblBorrowerAmount.text = amount[indexPath.row]
        cell.lblBorrowerDes.text = des[indexPath.row]
        cell.lblBorrowerDate.text = date[indexPath.row]

        cell.layer.cornerRadius = 25
        return cell
    }
    
    
}
