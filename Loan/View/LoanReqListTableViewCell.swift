//
//  LoanReqListTableViewCell.swift
//  Loan
//
//  Created by Faique Ali on 26/08/2020.
//  Copyright © 2020 Faique Ali. All rights reserved.
//

import UIKit

class LoanReqListTableViewCell: UITableViewCell {

    @IBOutlet weak var lblBorrowerName: UILabel!
    @IBOutlet weak var lblBorrowerDes: UILabel!
    @IBOutlet weak var lblBorrowerAmount: UILabel!
    @IBOutlet weak var lblBorrowerDate: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
