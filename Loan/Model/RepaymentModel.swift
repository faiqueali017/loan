//
//  RepaymentModel.swift
//  Loan
//
//  Created by Faique Ali on 25/08/2020.
//  Copyright © 2020 Faique Ali. All rights reserved.
//

class RepaymentModel {
    
    var date: String? //PK
    var amount: String?
    
    init(date: String?, amount: String?) {
        self.date = date
        self.amount = amount
    }
    
}
