//
//  LoanRequestModel.swift
//  Loan
//
//  Created by Faique Ali on 25/08/2020.
//  Copyright © 2020 Faique Ali. All rights reserved.
//

class LoanRequestModel {
    
    var date: String?  //PK
    var borrowID: String?  //FK
    var deadline: String?
    var amount: String?
    var description: String?
    var payday: String?
    
    init(date: String?, borrowID: String?, deadline: String?, amount: String?, description: String?, payday: String?) {
        self.date = date
        self.borrowID = borrowID
        self.deadline = deadline
        self.amount = amount
        self.description = description
        self.payday = payday
    }
    
    init(amount: String?, description: String?) {
        self.amount = amount
        self.description = description
    }
    
}
