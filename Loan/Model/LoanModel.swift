//
//  LoanModel.swift
//  Loan
//
//  Created by Faique Ali on 25/08/2020.
//  Copyright © 2020 Faique Ali. All rights reserved.
//

class LoanModel {
    
    var date: String? //PK
    var deadlineAgreedDate: String? //FK
    var repaymentDate: String? //FK
    
    init(date: String?, deadlineAgreedDate: String?, repaymentDate: String?) {
        self.date = date
        self.deadlineAgreedDate = deadlineAgreedDate
        self.repaymentDate = repaymentDate
    }
    
}
