//
//  LoanRequestLenderModel.swift
//  Loan
//
//  Created by Faique Ali on 25/08/2020.
//  Copyright © 2020 Faique Ali. All rights reserved.
//

class LoanRequestLenderModel {
    
    var loanRequestDate: String? //PK
    var lenderID: String? //FK
    var amount: Int?
    
    init(loanRequestDate: String?, lenderID: String?, amount: Int?) {
        self.loanRequestDate = loanRequestDate
        self.lenderID = lenderID
        self.amount = amount
    }
    
    init(loanRequestDate: String?) {
        self.loanRequestDate = loanRequestDate
    }
}
