//
//  DeadlineModel.swift
//  Loan
//
//  Created by Faique Ali on 25/08/2020.
//  Copyright © 2020 Faique Ali. All rights reserved.
//

class DeadlineModel {
    
    var agreedDate: String? //PK
    
    init(agreedDate: String?) {
        self.agreedDate = agreedDate
    }
    
}
