//
//  Addressee.swift
//  Loan
//
//  Created by Faique Ali on 25/08/2020.
//  Copyright © 2020 Faique Ali. All rights reserved.
//

class AddresseeModel {
    
    var id: String?
    var name: String?
    var address: String?
    
    init(id: String?, name: String?, address: String?) {
        self.id = id
        self.name = name
        self.address = address
    }
    
    init(name: String?) {
        self.name = name
    }
}
