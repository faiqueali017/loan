//
//  LenderModel.swift
//  Loan
//
//  Created by Faique Ali on 25/08/2020.
//  Copyright © 2020 Faique Ali. All rights reserved.
//

class LenderModel {
    
    var id: String? //PK
    var addresseeID: String? // FK
    
    init(id: String?, addresseeID: String?) {
        self.id = id
        self.addresseeID = addresseeID
    }
    
}
