//
//  LenderBorrowerModel.swift
//  Loan
//
//  Created by Faique Ali on 25/08/2020.
//  Copyright © 2020 Faique Ali. All rights reserved.
//

class LenderBorrowerModel {
    
    var borrowerID: String? //FK
    var lenderID: String? //FK
    var loanDate: String? //FK
    var percentage: Int?
    
    init(borrowerID: String?, lenderID: String?, loanDate: String?, percentage: Int?) {
        self.borrowerID = borrowerID
        self.lenderID = lenderID
        self.loanDate = loanDate
        self.percentage = percentage
    }
    
    
}
